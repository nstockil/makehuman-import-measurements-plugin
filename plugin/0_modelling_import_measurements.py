#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

"""
**Project Name:**      MakeHuman

**Product Home Page:** http://www.makehuman.org/

**Code Home Page:**    https://bitbucket.org/MakeHuman/makehuman/

**Authors:**           Manuel Bastioni, Marc Flerackers, Jonas Hauquier

**Copyright(c):**      MakeHuman Team 2001-2015

**Licensing:**         AGPL3 (http://www.makehuman.org/doc/node/the_makehuman_application.html)

    This file is part of MakeHuman (www.makehuman.org).

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

**Coding Standards:**  See http://www.makehuman.org/node/165

Abstract
--------
Import/Export module that uses JSON text to read in values for modifiers.

TODO
- get undo/redo functioning
"""

import gui3d
import gui
from core import G
import mh
import log
import os
from codecs import open
import json

class ImportAction(gui3d.Action):
    def __init__(self, human, before, after):
        super(ImportAction, self).__init__("Import")
        self.human = human
        self.before = before
        self.after = after

    def do(self):
        self._assignModifierValues(self.after)
        return True

    def undo(self):
        self._assignModifierValues(self.before)
        return True

    def _assignModifierValues(self, valuesDict):
        _tmp = self.human.symmetryModeEnabled
        self.human.symmetryModeEnabled = False
        for mName, val in valuesDict.items():
            try:
                self.human.getModifier(mName).setValue(val)
            except:
                pass
        self.human.applyAllTargets()
        self.human.symmetryModeEnabled = _tmp

class ImportTaskView(gui3d.TaskView):

    def __init__(self, category):
        gui3d.TaskView.__init__(self, category, 'Import Measurements')
        self.human = G.app.selectedHuman
        toolbox = self.addLeftWidget(gui.SliderBox('Import JSON'))
        self.loadButton = toolbox.addWidget(gui.BrowseButton(mode='open'), 0, 0)
        self.loadButton.setLabel('Load ...')
        self.loadButton.directory = mh.getPath()
        self.saveAsButton = toolbox.addWidget(gui.Button('Save As...'), 2, 0)
        
        @self.loadButton.mhEvent
        def onClicked(filename):
            if not filename:
                return
            if(os.path.exists(filename)):
                # read in json information
                contents = open(filename, 'rU', encoding="utf-8").read()
                log.debug('File contents read')       
                newMeasurements = json.loads(contents)    
                # validate/process the new measurements
                #oldValues = dict( [(m.fullName, m.getValue()) for m in self.human.modifiers] )
                newValues, oldValues = processMeaurements(self.human,  newMeasurements)
                log.debug("File imported for measurements")
                # apply new measurements
                gui3d.app.do( ImportAction(self.human, oldValues, newValues) )
                log.debug("Imported measurements have been applied")
                self.filename = filename
                self.directory = os.path.split(filename)[0]
            else:
                log.debug("Imported measurements have been not been applied, file does not exist")

        @self.saveAsButton.mhEvent
        def onClicked(event):
            filename = mh.getSaveFileName(
                mh.getPath(),
                'JSON File (*.json);;All files (*.*)')
            if filename:
                doSave(filename)
        
def doSave(filename):
    # get modifier information as JSON
    currentValues = dict( [(m.fullName.encode('ascii','replace'), m.getValue()) for m in G.app.selectedHuman.modifiers] )
    # save out JSON data
    with open(filename, 'w') as outfile:
        json.dump(currentValues, outfile, indent=4, sort_keys=True)
    log.debug("Measurements information saved to %s" % filename)

def processMeaurements(human,  newMeasurements) :
    oldValues = dict( [(m.fullName, m.getValue()) for m in human.modifiers] )
    newValues = oldValues
    
    # update values in newValues to reflect new measurements
    for mod, v in newMeasurements.items():
        m = human.getModifier(mod)
        if v < m.getMin() or v > m.getMax():
            newValue = m.getDefaultValue()
        else:
            newValue = v
        newValues[m.fullName] = newValue
        # apply value to opposite side, if it exists
        symm = m.getSymmetricOpposite()
        if symm and symm not in newValues:
            m2 = human.getModifier(symm)
            if v < m2.getMin() or v > m2.getMax():
                newValue = m2.getDefaultValue()
            else:
                newValue = v
            newValues[m2.fullName] = newValue

    # prevent pregnancy for male, too young or too old subjects
    if newValues.get("macrodetails/Gender", 0) > 0.5 or \
       newValues.get("macrodetails/Age", 0.5) < 0.2 or \
       newValues.get("macrodetails/Age", 0.7) < 0.75:
        if "stomach/stomach-pregnant-decr|incr" in newValues:
            newValues["stomach/stomach-pregnant-decr|incr"] = 0

    # prevent total ehtnicity values being greater than 1 (linked together)
    ehtnicities = ["macrodetails/African", "macrodetails/Asian", "macrodetails/Caucasian"]
    totalEthnicity = newValues["macrodetails/African"] + newValues["macrodetails/Asian"] + newValues["macrodetails/Caucasian"]
    log.debug(totalEthnicity)
    
    if totalEthnicity == 0:
        # reset ehtnicities to default values
        for e in ehtnicities:
            mod = human.getModifier(e)
            newValues[mod.fullName] = mod.getDefaultValue()
    elif totalEthnicity != 1:
        log.debug('not equal to 1')
         #scale each ethnicity to a valid value
        for e in ehtnicities:
            adjustedValue = newValues[e] * (1.0/ totalEthnicity)
            newValues[e] = adjustedValue

    #return new measurements
    return newValues, oldValues

# used for scaling ehtnicities to valid values
def translate(value, leftMin, leftMax, rightMin, rightMax):
    # Figure out how 'wide' each range is
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin

    # Convert the left range into a 0-1 range (float)
    valueScaled = float(value - leftMin) / float(leftSpan)

    # Convert the 0-1 range into a value in the right range.
    return rightMin + (valueScaled * rightSpan)

def load(app):
    category = app.getCategory('Modelling')
    taskview = category.addTask(ImportTaskView(category))

def unload(app):
    pass